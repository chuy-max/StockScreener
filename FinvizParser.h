#ifndef FINVIZPARSER_H
#define FINVIZPARSER_H

#include <vector>
#include <string>
#include "Company.h"
#include "Selection.h"
#include "Document.h"
#include "FinvizField.h"

class FinvizParser
{
public:
    static std::vector<Company> getCompaniesFromWebPage(const std::string webPage);
private:
    static CSelection getRows(CDocument &doc);
    static Company getCompanyFromRow(CNode &rowNode);
    static std::string getStringFromRow(CSelection &cellSelection, FinvizFields field);
    static Decimal getDecimalFromRow(CSelection &cellSelection, FinvizFields field, FieldDefaultValue defaultValue);
    static void removeInvalidDecimalCharacters(std::string &s);
};

#endif // FINVIZPARSER_H
