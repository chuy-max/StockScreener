#ifndef COMPANYMAPPER_H
#define COMPANYMAPPER_H

#include <QVariant>
#include "Company.h"

class CompanyMapper
{
public:
    static QVariant getFieldAsVariant(size_t index, const Company &c);
    static QVariant getFieldName(size_t index);
    static size_t fieldCount;
};

#endif // COMPANYMAPPER_H
