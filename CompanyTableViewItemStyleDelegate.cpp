#include "CompanyTableViewItemStyleDelegate.h"
#include "Money.h"

CompanyTableViewItemStyleDelegate::CompanyTableViewItemStyleDelegate(QObject *parent) :
    QStyledItemDelegate(parent)
{
}

QString CompanyTableViewItemStyleDelegate::displayText(const QVariant & value, const QLocale & locale) const
{
    if(value.type() == QVariant::Double) {
        if(QVariant(Money::getInfinity().getAsDouble()) == value) {
            return "INF";
        }
        else {
            QString str = locale.toCurrencyString(value.toDouble());
            str = str.replace("$", "");
            return str;
        }
    }

    return value.toString();
}
