#include "FinvizDownloader.h"
#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>
#include <sstream>

std::string FinvizDownloader::finvizUrl = "https://finviz.com/screener.ashx?v=152&ft=4&c=0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70&r=";

std::string FinvizDownloader::getWebPage(size_t offset)
{
    std::ostringstream response;
    curlpp::Easy request;

    // URL starts at 1 (not 0). 20 companies per page.
    std::string url = finvizUrl + std::to_string(offset*20+1);

    request.setOpt<curlpp::options::WriteStream>(&response);
    request.setOpt<curlpp::options::Url>(url);
    //request.setOpt<curlpp::options::Verbose>(true);
    request.setOpt<curlpp::options::UserAgent>("Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_3_3 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8J2 Safari/6533.18.5");
    request.perform();

    return response.str();
}

size_t FinvizDownloader::getPageCount()
{
    return 352;
}
