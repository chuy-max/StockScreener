#ifndef COMPANIESMODEL_H
#define COMPANIESMODEL_H

#include <QAbstractTableModel>
#include "Company.h"
#include <string>
#include <vector>

class CompanyTableModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    CompanyTableModel(QObject *parent, std::vector<Company> companies);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
private:
    std::vector<Company> companies;
};

#endif // COMPANIESMODEL_H
