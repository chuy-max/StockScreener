#include "StockScreener.h"
#include "ui_stockscreener.h"
#include "CompanySortFilterProxymodel.h"

StockScreener::StockScreener(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::StockScreener)
{
    ui->setupUi(this);
    ui->tableView->initialize();

    connect(ui->maxPESpinBox, SIGNAL (valueChanged(double)),
            ui->tableView->getCompaniesProxyModel(), SLOT (setMaxPE(double)) );
    connect(ui->minCurrentRatioSpinBox, SIGNAL (valueChanged(double)),
            ui->tableView->getCompaniesProxyModel(), SLOT (setMinCurrentRatio(double)) );
    connect(ui->minDividendSpinBox, SIGNAL (valueChanged(double)),
            ui->tableView->getCompaniesProxyModel(), SLOT (setMinDividend(double)) );
    connect(ui->maxPBSpinBox, SIGNAL (valueChanged(double)),
            ui->tableView->getCompaniesProxyModel(), SLOT (setMaxPB(double)) );
    connect(ui->actionQuit, SIGNAL(triggered(bool)), SLOT(close()));
}

StockScreener::~StockScreener()
{
    delete ui;
}
