#include "Money.h"
#include <cctype>
#include <sstream>

static bool isNumericSymbol(char symbol)
{
    return isdigit(symbol) || symbol=='-' || symbol=='.';
}

static bool isNumber(const std::string &s)
{
    std::string::const_iterator it = s.begin();
    while (it != s.end() && isNumericSymbol(*it)) {
        ++it;
    }
    return !s.empty() && it == s.end();
}

static bool isInvalidMoneyString(const std::string &decimalValue)
{
    return decimalValue=="" || decimalValue=="-" || !isNumber(decimalValue);
}

Money::Money()
{
}

Money::Money(std::string decimalValue) :
    decimal<4>(decimalValue)
{
    if(isInvalidMoneyString(decimalValue))
    {
        std::ostringstream stream;
        stream << __FUNCTION__ << ": Invalid numeric value: " << decimalValue;
        throw MoneyException(stream.str());
    }
}

bool Money::operator==(const Money &rhs) const
{
    return decimal::operator==(rhs);
}

Money& Money::operator*=(const Money &rhs)
{
    decimal::operator*=(rhs);
    return *this;
}

Money Money::getInfinity()
{
    Money infinity;
    infinity.setUnbiased(LLONG_MAX);
    return infinity;
}
Money Money::getNegativeInfinity()
{
    Money negativeInfinity;
    negativeInfinity.setUnbiased(LLONG_MIN);
    return negativeInfinity;
}

long long& Money::getRawDecimal()
{
    return m_value;
}
