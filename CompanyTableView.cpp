#include <QHeaderView>

#include "CompanyTableView.h"
#include "CompanyTableModel.h"
#include "CompanySortFilterProxymodel.h"
#include "Company.h"
#include "CompanyDatabase.h"
#include "CompanyTableViewItemStyleDelegate.h"

CompanyTableView::CompanyTableView(QWidget *parent) :
    QTableView(parent)
{
}

void CompanyTableView::initialize(void)
{
    horizontalHeader()->setStretchLastSection(true);
    setSortingEnabled(true);
    setAlternatingRowColors(true);

    CompanyTableViewItemStyleDelegate *styleDelegate = new CompanyTableViewItemStyleDelegate(this);
    setItemDelegate(styleDelegate);

    std::vector<Company> companies = CompanyDatabase().load();
    CompanyTableModel *sourceModel = new CompanyTableModel(this, companies);
    proxyModel = new CompanySortFilterProxyModel(this);
    proxyModel->setSourceModel(sourceModel);
    setModel(proxyModel); //setModel(proxyModel);
}

CompanySortFilterProxyModel* CompanyTableView::getCompaniesProxyModel(void) const
{
    return proxyModel;
}
