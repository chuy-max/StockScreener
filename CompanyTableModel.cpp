#include "CompanyTableModel.h"
#include "CompanyMapper.h"

CompanyTableModel::CompanyTableModel(QObject *parent, std::vector<Company> companies) :
    QAbstractTableModel(parent),
    companies(companies)
{
}

int CompanyTableModel::rowCount(const QModelIndex & /*parent*/) const
{
   return static_cast<int>(companies.size());
}

int CompanyTableModel::columnCount(const QModelIndex & /*parent*/) const
{
    return static_cast<int>(CompanyMapper::fieldCount);
}

QVariant CompanyTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole) {
        if (orientation == Qt::Horizontal) {
            return CompanyMapper::getFieldName(section);
        }
    }
    return QAbstractTableModel::headerData(section, orientation, role);
}

QVariant CompanyTableModel::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole && index.row()>=0) {
        return CompanyMapper::getFieldAsVariant(index.column(),
                                                companies[index.row()]);
    }
    return QVariant();
}
