#include "FinvizScraper.h"
#include <Windows.h>
#include "FinvizDownloader.h"
#include "FinvizParser.h"

std::vector<Company> FinvizScraper::getTodaysCompanyData()
{
    std::vector<Company> companies;

    for(int i=0; i<FinvizDownloader::getPageCount(); i++) {
        printf("Downloading page: %d\n", i);
        fflush(stdout);

        std::string doc = FinvizDownloader::getWebPage(i);
        std::vector<Company> companiesForPage = FinvizParser::getCompaniesFromWebPage(doc);
        companies.insert( companies.end(), companiesForPage.begin(), companiesForPage.end() );
        Sleep(3000);
    }

    return companies;
}
