#include "CompanyDatabase.h"
#include <cereal/archives/binary.hpp>
#include <cereal/types/vector.hpp>
#include <fstream>

static std::string databaseFile = "C:\\Users\\Max\\dev\\temp\\companies.dat";

void CompanyDatabase::store(const std::vector<Company> &companies)
{
    std::ofstream outputStream(databaseFile, std::ios::binary);

    {
        cereal::BinaryOutputArchive oarchive(outputStream);
        oarchive(companies);
    }
}

std::vector<Company> CompanyDatabase::load()
{
    std::vector<Company> companies;

    std::ifstream inputStream(databaseFile, std::ios::binary);
    {
        cereal::BinaryInputArchive iarchive(inputStream);
        iarchive(companies);
    }

    return companies;
}
