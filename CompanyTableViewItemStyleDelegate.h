#ifndef COMPANYTABLEVIEWITEMSTYLEDELEGATE_H
#define COMPANYTABLEVIEWITEMSTYLEDELEGATE_H

#include <QStyledItemDelegate>

class CompanyTableViewItemStyleDelegate : public QStyledItemDelegate
{
   Q_OBJECT
public:
   CompanyTableViewItemStyleDelegate(QObject *parent = 0);

   QString displayText(const QVariant & value, const QLocale & locale) const;
};

#endif // COMPANYTABLEVIEWITEMSTYLEDELEGATE_H
