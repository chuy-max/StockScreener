#ifndef COMPANIESTABLEVIEW_H
#define COMPANIESTABLEVIEW_H

#include <QTableView>
#include <CompanySortFilterProxymodel.h>

class CompanyTableView : public QTableView
{
public:
    CompanyTableView(QWidget *parent);
    void initialize(void);
    CompanySortFilterProxyModel* getCompaniesProxyModel(void) const;

private:
    CompanySortFilterProxyModel *proxyModel;
};

#endif // COMPANIESTABLEVIEW_H
