#ifndef FINVIZSCRAPER_H
#define FINVIZSCRAPER_H

#include <vector>
#include "Company.h"

class FinvizScraper
{
public:
    static std::vector<Company> getTodaysCompanyData();
};

#endif // FINVIZSCRAPER_H
