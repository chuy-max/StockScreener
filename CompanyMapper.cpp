#include "CompanyMapper.h"
#include <exception>

size_t CompanyMapper::fieldCount = 70;

QVariant CompanyMapper::getFieldAsVariant(size_t index, const Company &c)
{
    switch (index) {
    case 0: return QVariant(QString::fromStdString(c.ticker)); break;
    case 1: return QVariant(QString::fromStdString(c.name)); break;
    case 2: return QVariant(QString::fromStdString(c.sector)); break;
    case 3: return QVariant(QString::fromStdString(c.industry)); break;
    case 4: return QVariant(QString::fromStdString(c.country)); break;
    case 5: return QVariant(c.marketCap.getAsDouble()); break;
    case 6: return QVariant(c.priceToEarnings.getAsDouble()); break;
    case 7: return QVariant(c.forwardPriceToEarnings.getAsDouble()); break;
    case 8: return QVariant(c.priceEarningsToGrowth.getAsDouble()); break;
    case 9: return QVariant(c.priceToSales.getAsDouble()); break;
    case 10: return QVariant(c.priceToBook.getAsDouble()); break;
    case 11: return QVariant(c.putCall.getAsDouble()); break;
    case 12: return QVariant(c.pFCF.getAsDouble()); break;
    case 13: return QVariant(c.dividendPercentage.getAsDouble()); break;
    case 14: return QVariant(c.payoutRatio.getAsDouble()); break;
    case 15: return QVariant(c.earningsPerShare.getAsDouble()); break;
    case 16: return QVariant(c.earningsPerShareThisYear.getAsDouble()); break;
    case 17: return QVariant(c.earningsPerShareNextYear.getAsDouble()); break;
    case 18: return QVariant(c.earningsPerSharePast5Years.getAsDouble()); break;
    case 19: return QVariant(c.earningsPerShareNext5Years.getAsDouble()); break;
    case 20: return QVariant(c.salesPast5Years.getAsDouble()); break;
    case 21: return QVariant(c.earningsPerShareQuarterToQuarter.getAsDouble()); break;
    case 22: return QVariant(c.salesQuarterToQuarter.getAsDouble()); break;
    case 23: return QVariant(c.outstanding.getAsDouble()); break;
    case 24: return QVariant(c.floatShares.getAsDouble()); break;
    case 25: return QVariant(c.insiderOwn.getAsDouble()); break;
    case 26: return QVariant(c.insiderTrans.getAsDouble()); break;
    case 27: return QVariant(c.instOwn.getAsDouble()); break;
    case 28: return QVariant(c.instTrans.getAsDouble()); break;
    case 29: return QVariant(c.floatShort.getAsDouble()); break;
    case 30: return QVariant(c.shortRatio.getAsDouble()); break;
    case 31: return QVariant(c.returnOnAssets.getAsDouble()); break;
    case 32: return QVariant(c.returnOnEquity.getAsDouble()); break;
    case 33: return QVariant(c.returnOnInvestment.getAsDouble()); break;
    case 34: return QVariant(c.currentRatio.getAsDouble()); break;
    case 35: return QVariant(c.quickRatio.getAsDouble()); break;
    case 36: return QVariant(c.longTermDebtToEquity.getAsDouble()); break;
    case 37: return QVariant(c.debtToEquity.getAsDouble()); break;
    case 38: return QVariant(c.grossM.getAsDouble()); break;
    case 39: return QVariant(c.operM.getAsDouble()); break;
    case 40: return QVariant(c.profitM.getAsDouble()); break;
    case 41: return QVariant(c.perfWeek.getAsDouble()); break;
    case 42: return QVariant(c.perfMonth.getAsDouble()); break;
    case 43: return QVariant(c.perfQuart.getAsDouble()); break;
    case 44: return QVariant(c.perfHalf.getAsDouble()); break;
    case 45: return QVariant(c.perfYear.getAsDouble()); break;
    case 46: return QVariant(c.perfYTD.getAsDouble()); break;
    case 47: return QVariant(c.beta.getAsDouble()); break;
    case 48: return QVariant(c.aTR.getAsDouble()); break;
    case 49: return QVariant(c.volatilityW.getAsDouble()); break;
    case 50: return QVariant(c.volatilityM.getAsDouble()); break;
    case 51: return QVariant(c.sMA20.getAsDouble()); break;
    case 52: return QVariant(c.sMA50.getAsDouble()); break;
    case 53: return QVariant(c.sMA200.getAsDouble()); break;
    case 54: return QVariant(c.x50DHigh.getAsDouble()); break;
    case 55: return QVariant(c.x50DLow.getAsDouble()); break;
    case 56: return QVariant(c.x52WHigh.getAsDouble()); break;
    case 57: return QVariant(c.x52WLow.getAsDouble()); break;
    case 58: return QVariant(c.rSI.getAsDouble()); break;
    case 59: return QVariant(c.fromOpen.getAsDouble()); break;
    case 60: return QVariant(c.gap.getAsDouble()); break;
    case 61: return QVariant(c.recom.getAsDouble()); break;
    case 62: return QVariant(c.avgVolume.getAsDouble()); break;
    case 63: return QVariant(c.relVolume.getAsDouble()); break;
    case 64: return QVariant(c.price.getAsDouble()); break;
    case 65: return QVariant(c.change.getAsDouble()); break;
    case 66: return QVariant(c.volume.getAsDouble()); break;
    case 67: return QVariant(QString::fromStdString(c.earnings)); break;
    case 68: return QVariant(c.targetPrice.getAsDouble()); break;
    case 69: return QVariant(QString::fromStdString(c.iPODate)); break;
    }
    throw std::runtime_error("CompanyMapper::getFieldAsVariant. Exceeding column size");
}

QVariant CompanyMapper::getFieldName(size_t index)
{
    switch(index) {
    case 0: return QVariant("Ticker"); break;
    case 1: return QVariant("Name"); break;
    case 2: return QVariant("Sector"); break;
    case 3: return QVariant("Industry"); break;
    case 4: return QVariant("Country"); break;
    case 5: return QVariant("MarketCap"); break;
    case 6: return QVariant("PriceToEarnings"); break;
    case 7: return QVariant("ForwardPriceToEarnings"); break;
    case 8: return QVariant("PriceEarningsToGrowth"); break;
    case 9: return QVariant("PriceToSales"); break;
    case 10: return QVariant("PriceToBook"); break;
    case 11: return QVariant("PutCall"); break;
    case 12: return QVariant("PFCF"); break;
    case 13: return QVariant("DividendPercentage"); break;
    case 14: return QVariant("PayoutRatio"); break;
    case 15: return QVariant("EarningsPerShare"); break;
    case 16: return QVariant("EarningsPerShareThisYear"); break;
    case 17: return QVariant("EarningsPerShareNextYear"); break;
    case 18: return QVariant("EarningsPerSharePast5Years"); break;
    case 19: return QVariant("EarningsPerShareNext5Years"); break;
    case 20: return QVariant("SalesPast5Years"); break;
    case 21: return QVariant("EarningsPerShareQuarterToQuarter"); break;
    case 22: return QVariant("SalesQuarterToQuarter"); break;
    case 23: return QVariant("Outstanding"); break;
    case 24: return QVariant("FloatShares"); break;
    case 25: return QVariant("InsiderOwn"); break;
    case 26: return QVariant("InsiderTrans"); break;
    case 27: return QVariant("InstOwn"); break;
    case 28: return QVariant("InstTrans"); break;
    case 29: return QVariant("FloatShort"); break;
    case 30: return QVariant("ShortRatio"); break;
    case 31: return QVariant("ReturnOnAssets"); break;
    case 32: return QVariant("ReturnOnEquity"); break;
    case 33: return QVariant("ReturnOnInvestment"); break;
    case 34: return QVariant("CurrentRatio"); break;
    case 35: return QVariant("QuickRatio"); break;
    case 36: return QVariant("LongTermDebtToEquity"); break;
    case 37: return QVariant("DebtToEquity"); break;
    case 38: return QVariant("GrossM"); break;
    case 39: return QVariant("OperM"); break;
    case 40: return QVariant("ProfitM"); break;
    case 41: return QVariant("PerfWeek"); break;
    case 42: return QVariant("PerfMonth"); break;
    case 43: return QVariant("PerfQuart"); break;
    case 44: return QVariant("PerfHalf"); break;
    case 45: return QVariant("PerfYear"); break;
    case 46: return QVariant("PerfYTD"); break;
    case 47: return QVariant("Beta"); break;
    case 48: return QVariant("ATR"); break;
    case 49: return QVariant("VolatilityW"); break;
    case 50: return QVariant("VolatilityM"); break;
    case 51: return QVariant("SMA20"); break;
    case 52: return QVariant("SMA50"); break;
    case 53: return QVariant("SMA200"); break;
    case 54: return QVariant("X50DHigh"); break;
    case 55: return QVariant("X50DLow"); break;
    case 56: return QVariant("X52WHigh"); break;
    case 57: return QVariant("X52WLow"); break;
    case 58: return QVariant("RSI"); break;
    case 59: return QVariant("FromOpen"); break;
    case 60: return QVariant("Gap"); break;
    case 61: return QVariant("Recom"); break;
    case 62: return QVariant("AvgVolume"); break;
    case 63: return QVariant("RelVolume"); break;
    case 64: return QVariant("Price"); break;
    case 65: return QVariant("Change"); break;
    case 66: return QVariant("Volume"); break;
    case 67: return QVariant("Earnings"); break;
    case 68: return QVariant("TargetPrice"); break;
    case 69: return QVariant("IPODate"); break;
    }
    throw std::runtime_error("CompanyMapper::getFieldName. Exceeding column size");
}
