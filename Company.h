#ifndef COMPANY_H
#define COMPANY_H

#include <string>
#include "Money.h"
#include <cereal/types/string.hpp>

// If you change a Company field, make sure you change the serialize function
// and CompanyMapper class. Also take a look at FinvizField.h, things might
// change here too.

struct Company
{
    std::string toString() {
        std::ostringstream os;
        os << ticker << " " << name << " " << price;
        return os.str();
    }

    std::string ticker;
    std::string name;
    std::string sector;
    std::string industry;
    std::string country;
    Money marketCap;
    Decimal priceToEarnings;
    Decimal forwardPriceToEarnings;
    Decimal priceEarningsToGrowth;
    Decimal priceToSales;
    Decimal priceToBook;
    Decimal putCall;
    Decimal pFCF;
    Decimal dividendPercentage;
    Decimal payoutRatio;
    Decimal earningsPerShare;
    Decimal earningsPerShareThisYear;
    Decimal earningsPerShareNextYear;
    Decimal earningsPerSharePast5Years;
    Decimal earningsPerShareNext5Years;
    Decimal salesPast5Years;
    Decimal earningsPerShareQuarterToQuarter;
    Decimal salesQuarterToQuarter;
    Decimal outstanding;
    Decimal floatShares;
    Decimal insiderOwn;
    Decimal insiderTrans;
    Decimal instOwn;
    Decimal instTrans;
    Decimal floatShort;
    Decimal shortRatio;
    Decimal returnOnAssets;
    Decimal returnOnEquity;
    Decimal returnOnInvestment;
    Decimal currentRatio;
    Decimal quickRatio;
    Decimal longTermDebtToEquity;
    Decimal debtToEquity;
    Decimal grossM;
    Decimal operM;
    Decimal profitM;
    Decimal perfWeek;
    Decimal perfMonth;
    Decimal perfQuart;
    Decimal perfHalf;
    Decimal perfYear;
    Decimal perfYTD;
    Decimal beta;
    Decimal aTR;
    Decimal volatilityW;
    Decimal volatilityM;
    Decimal sMA20;
    Decimal sMA50;
    Decimal sMA200;
    Decimal x50DHigh;
    Decimal x50DLow;
    Decimal x52WHigh;
    Decimal x52WLow;
    Decimal rSI;
    Decimal fromOpen;
    Decimal gap;
    Decimal recom;
    Decimal avgVolume;
    Decimal relVolume;
    Decimal price;
    Decimal change;
    Decimal volume;
    std::string earnings;
    Decimal targetPrice;
    std::string iPODate;
};

class CompanyFields {
public:
    enum CompanyFieldsEnum
    {
        //Number,
        Ticker = 0,
        Name,
        Sector,
        Industry,
        Country,
        MarketCap,
        PriceToEarnings,
        ForwardPriceToEarnings,
        PriceEarningsToGrowth,
        PriceToSales,
        PriceToBook,
        PutCall,
        PFCF,
        DividendPercentage,
        PayoutRatio,
        EarningsPerShare,
        EarningsPerShareThisYear,
        EarningsPerShareNextYear,
        EarningsPerSharePast5Years,
        EarningsPerShareNext5Years,
        SalesPast5Years,
        EarningsPerShareQuarterToQuarter,
        SalesQuarterToQuarter,
        Outstanding,
        FloatShares,
        InsiderOwn,
        InsiderTrans,
        InstOwn,
        InstTrans,
        FloatShort,
        ShortRatio,
        ReturnOnAssets,
        ReturnOnEquity,
        ReturnOnInvestment,
        CurrentRatio,
        QuickRatio,
        LongTermDebtToEquity,
        DebtToEquity,
        GrossM,
        OperM,
        ProfitM,
        PerfWeek,
        PerfMonth,
        PerfQuart,
        PerfHalf,
        PerfYear,
        PerfYTD,
        Beta,
        ATR,
        VolatilityW,
        VolatilityM,
        SMA20,
        SMA50,
        SMA200,
        x50DHigh,
        x50DLow,
        x52WHigh,
        x52WLow,
        RSI,
        FromOpen,
        Gap,
        Recom,
        AvgVolume,
        RelVolume,
        Price,
        Change,
        Volume,
        Earnings,
        TargetPrice,
        IPODate
    };
};

template <class Archive>
void serialize(Archive & ar,
               Company & c)
{
    ar(c.ticker,
       c.name,
       c.sector,
       c.industry,
       c.country,
       c.marketCap,
       c.priceToEarnings,
       c.forwardPriceToEarnings,
       c.priceEarningsToGrowth,
       c.priceToSales,
       c.priceToBook,
       c.putCall,
       c.pFCF,
       c.dividendPercentage,
       c.payoutRatio,
       c.earningsPerShare,
       c.earningsPerShareThisYear,
       c.earningsPerShareNextYear,
       c.earningsPerSharePast5Years,
       c.earningsPerShareNext5Years,
       c.salesPast5Years,
       c.earningsPerShareQuarterToQuarter,
       c.salesQuarterToQuarter,
       c.outstanding,
       c.floatShares,
       c.insiderOwn,
       c.insiderTrans,
       c.instOwn,
       c.instTrans,
       c.floatShort,
       c.shortRatio,
       c.returnOnAssets,
       c.returnOnEquity,
       c.returnOnInvestment,
       c.currentRatio,
       c.quickRatio,
       c.longTermDebtToEquity,
       c.debtToEquity,
       c.grossM,
       c.operM,
       c.profitM,
       c.perfWeek,
       c.perfMonth,
       c.perfQuart,
       c.perfHalf,
       c.perfYear,
       c.perfYTD,
       c.beta,
       c.aTR,
       c.volatilityW,
       c.volatilityM,
       c.sMA20,
       c.sMA50,
       c.sMA200,
       c.x50DHigh,
       c.x50DLow,
       c.x52WHigh,
       c.x52WLow,
       c.rSI,
       c.fromOpen,
       c.gap,
       c.recom,
       c.avgVolume,
       c.relVolume,
       c.price,
       c.change,
       c.volume,
       c.earnings,
       c.targetPrice,
       c.iPODate);
}

#endif // COMPANY_H
