#include "FinvizParser.h"
#include "Document.h"
#include "Selection.h"
#include "Node.h"
#include <regex>

static int RowsPerPage = 21;
static int ColumnsPerPage = 71;
static Decimal OneBillion("1000000000");
static Decimal OneMillion("1000000");
static Decimal OneThousand("1000");

std::vector<Company> FinvizParser::getCompaniesFromWebPage(const std::string webPage)
{
    std::vector<Company> companies;

    CDocument doc;
    doc.parse(webPage.c_str());
    CSelection selection = getRows(doc);

    // i=0 is the header. Discard.
    for(int i=1; i<selection.nodeNum(); i++) {
        CNode row = selection.nodeAt(i);
        Company company = getCompanyFromRow(row);
        companies.push_back(company);
    }

    return companies;
}

CSelection FinvizParser::getRows(CDocument &doc)
{
    CSelection selection = doc.find("div#screener-content table[bgcolor=\"#d3d3d3\"] tbody tr");
    return selection;
}

Company FinvizParser::getCompanyFromRow(CNode &rowNode)
{
    CSelection cellSelection = rowNode.find("td");

    if(cellSelection.nodeNum() != ColumnsPerPage) {
        throw std::exception("FinvizParser::getCompanyFromRow: Columns retrieved from web page do not match layout.");
    }

    Company c;
    c.ticker = getStringFromRow(cellSelection, FinvizFields::Ticker);
    c.name = getStringFromRow(cellSelection, FinvizFields::Name);
    c.sector = getStringFromRow(cellSelection, FinvizFields::Sector);
    c.industry = getStringFromRow(cellSelection, FinvizFields::Industry);
    c.country = getStringFromRow(cellSelection, FinvizFields::Country);
    c.marketCap = getDecimalFromRow(cellSelection, FinvizFields::MarketCap, FieldDefaultValue::Zero);
    c.priceToEarnings = getDecimalFromRow(cellSelection, FinvizFields::PriceToEarnings, FieldDefaultValue::Infinity);
    c.forwardPriceToEarnings = getDecimalFromRow(cellSelection, FinvizFields::ForwardPriceToEarnings, FieldDefaultValue::Infinity);
    c.priceEarningsToGrowth = getDecimalFromRow(cellSelection, FinvizFields::PriceEarningsToGrowth, FieldDefaultValue::Zero);
    c.priceToSales = getDecimalFromRow(cellSelection, FinvizFields::PriceToSales, FieldDefaultValue::Infinity);
    c.priceToBook = getDecimalFromRow(cellSelection, FinvizFields::PriceToBook, FieldDefaultValue::Infinity);
    c.putCall = getDecimalFromRow(cellSelection, FinvizFields::PutCall, FieldDefaultValue::Zero);
    c.pFCF = getDecimalFromRow(cellSelection, FinvizFields::PFCF, FieldDefaultValue::Zero);
    c.dividendPercentage = getDecimalFromRow(cellSelection, FinvizFields::DividendPercentage, FieldDefaultValue::Zero);
    c.payoutRatio = getDecimalFromRow(cellSelection, FinvizFields::PayoutRatio, FieldDefaultValue::Zero);
    c.earningsPerShare = getDecimalFromRow(cellSelection, FinvizFields::EarningsPerShare, FieldDefaultValue::Zero);
    c.earningsPerShareThisYear = getDecimalFromRow(cellSelection, FinvizFields::EarningsPerShareThisYear, FieldDefaultValue::Zero);
    c.earningsPerShareNextYear = getDecimalFromRow(cellSelection, FinvizFields::EarningsPerShareNextYear, FieldDefaultValue::Zero);
    c.earningsPerSharePast5Years = getDecimalFromRow(cellSelection, FinvizFields::EarningsPerSharePast5Years, FieldDefaultValue::Zero);
    c.earningsPerShareNext5Years = getDecimalFromRow(cellSelection, FinvizFields::EarningsPerShareNext5Years, FieldDefaultValue::Zero);
    c.salesPast5Years = getDecimalFromRow(cellSelection, FinvizFields::SalesPast5Years, FieldDefaultValue::Zero);
    c.earningsPerShareQuarterToQuarter = getDecimalFromRow(cellSelection, FinvizFields::EarningsPerShareQuarterToQuarter, FieldDefaultValue::Zero);
    c.salesQuarterToQuarter = getDecimalFromRow(cellSelection, FinvizFields::SalesQuarterToQuarter, FieldDefaultValue::Zero);
    c.outstanding = getDecimalFromRow(cellSelection, FinvizFields::Outstanding, FieldDefaultValue::Zero);
    c.floatShares = getDecimalFromRow(cellSelection, FinvizFields::FloatShares, FieldDefaultValue::Zero);
    c.insiderOwn = getDecimalFromRow(cellSelection, FinvizFields::InsiderOwn, FieldDefaultValue::Zero);
    c.insiderTrans = getDecimalFromRow(cellSelection, FinvizFields::InsiderTrans, FieldDefaultValue::Zero);
    c.instOwn = getDecimalFromRow(cellSelection, FinvizFields::InstOwn, FieldDefaultValue::Zero);
    c.instTrans = getDecimalFromRow(cellSelection, FinvizFields::InstTrans, FieldDefaultValue::Zero);
    c.floatShort = getDecimalFromRow(cellSelection, FinvizFields::FloatShort, FieldDefaultValue::Zero);
    c.shortRatio = getDecimalFromRow(cellSelection, FinvizFields::ShortRatio, FieldDefaultValue::Zero);
    c.returnOnAssets = getDecimalFromRow(cellSelection, FinvizFields::ReturnOnAssets, FieldDefaultValue::Zero);
    c.returnOnEquity = getDecimalFromRow(cellSelection, FinvizFields::ReturnOnEquity, FieldDefaultValue::Zero);
    c.returnOnInvestment = getDecimalFromRow(cellSelection, FinvizFields::ReturnOnInvestment, FieldDefaultValue::Zero);
    c.currentRatio = getDecimalFromRow(cellSelection, FinvizFields::CurrentRatio, FieldDefaultValue::Zero);
    c.quickRatio = getDecimalFromRow(cellSelection, FinvizFields::QuickRatio, FieldDefaultValue::Zero);
    c.longTermDebtToEquity = getDecimalFromRow(cellSelection, FinvizFields::LongTermDebtToEquity, FieldDefaultValue::Zero);
    c.debtToEquity = getDecimalFromRow(cellSelection, FinvizFields::DebtToEquity, FieldDefaultValue::Zero);
    c.grossM = getDecimalFromRow(cellSelection, FinvizFields::GrossM, FieldDefaultValue::Zero);
    c.operM = getDecimalFromRow(cellSelection, FinvizFields::OperM, FieldDefaultValue::Zero);
    c.profitM = getDecimalFromRow(cellSelection, FinvizFields::ProfitM, FieldDefaultValue::Zero);
    c.perfWeek = getDecimalFromRow(cellSelection, FinvizFields::PerfWeek, FieldDefaultValue::Zero);
    c.perfMonth = getDecimalFromRow(cellSelection, FinvizFields::PerfMonth, FieldDefaultValue::Zero);
    c.perfQuart = getDecimalFromRow(cellSelection, FinvizFields::PerfQuart, FieldDefaultValue::Zero);
    c.perfHalf = getDecimalFromRow(cellSelection, FinvizFields::PerfHalf, FieldDefaultValue::Zero);
    c.perfYear = getDecimalFromRow(cellSelection, FinvizFields::PerfYear, FieldDefaultValue::Zero);
    c.perfYTD = getDecimalFromRow(cellSelection, FinvizFields::PerfYTD, FieldDefaultValue::Zero);
    c.beta = getDecimalFromRow(cellSelection, FinvizFields::Beta, FieldDefaultValue::Zero);
    c.aTR = getDecimalFromRow(cellSelection, FinvizFields::ATR, FieldDefaultValue::Zero);
    c.volatilityW = getDecimalFromRow(cellSelection, FinvizFields::VolatilityW, FieldDefaultValue::Zero);
    c.volatilityM = getDecimalFromRow(cellSelection, FinvizFields::VolatilityM, FieldDefaultValue::Zero);
    c.sMA20 = getDecimalFromRow(cellSelection, FinvizFields::SMA20, FieldDefaultValue::Zero);
    c.sMA50 = getDecimalFromRow(cellSelection, FinvizFields::SMA50, FieldDefaultValue::Zero);
    c.sMA200 = getDecimalFromRow(cellSelection, FinvizFields::SMA200, FieldDefaultValue::Zero);
    c.x50DHigh = getDecimalFromRow(cellSelection, FinvizFields::x50DHigh, FieldDefaultValue::Zero);
    c.x50DLow = getDecimalFromRow(cellSelection, FinvizFields::x50DLow, FieldDefaultValue::Zero);
    c.x52WHigh = getDecimalFromRow(cellSelection, FinvizFields::x52WHigh, FieldDefaultValue::Zero);
    c.x52WLow = getDecimalFromRow(cellSelection, FinvizFields::x52WLow, FieldDefaultValue::Zero);
    c.rSI = getDecimalFromRow(cellSelection, FinvizFields::RSI, FieldDefaultValue::Zero);
    c.fromOpen = getDecimalFromRow(cellSelection, FinvizFields::FromOpen, FieldDefaultValue::Zero);
    c.gap = getDecimalFromRow(cellSelection, FinvizFields::Gap, FieldDefaultValue::Zero);
    c.recom = getDecimalFromRow(cellSelection, FinvizFields::Recom, FieldDefaultValue::Zero);
    c.avgVolume = getDecimalFromRow(cellSelection, FinvizFields::AvgVolume, FieldDefaultValue::Zero);
    c.relVolume = getDecimalFromRow(cellSelection, FinvizFields::RelVolume, FieldDefaultValue::Zero);
    c.price = getDecimalFromRow(cellSelection, FinvizFields::Price, FieldDefaultValue::Zero);
    c.change = getDecimalFromRow(cellSelection, FinvizFields::Change, FieldDefaultValue::Zero);
    c.volume = getDecimalFromRow(cellSelection, FinvizFields::Volume, FieldDefaultValue::Zero);
    c.earnings = getStringFromRow(cellSelection, FinvizFields::Earnings);
    c.targetPrice = getDecimalFromRow(cellSelection, FinvizFields::TargetPrice, FieldDefaultValue::Zero);
    c.iPODate = getStringFromRow(cellSelection, FinvizFields::IPODate);

    return c;
}

std::string FinvizParser::getStringFromRow(CSelection &cellSelection, FinvizFields field)
{
    return cellSelection.nodeAt(field).text();
}

Decimal FinvizParser::getDecimalFromRow(CSelection &cellSelection, FinvizFields field, FieldDefaultValue defaultValue)
{
    std::string stringValue = cellSelection.nodeAt(field).text();
    Decimal multiplier("1");

    removeInvalidDecimalCharacters(stringValue);

    if(stringValue.find("B") != std::string::npos) {
        multiplier = OneBillion;
        stringValue = std::regex_replace(stringValue, std::regex("B"), "");
    }
    else if(stringValue.find("M") != std::string::npos) {
        multiplier = OneMillion;
        stringValue = std::regex_replace(stringValue, std::regex("M"), "");
    }
    else if(stringValue.find("K") != std::string::npos) {
        multiplier = OneMillion;
        stringValue = std::regex_replace(stringValue, std::regex("K"), "");
    }
    else if(stringValue == "-") {
        switch (defaultValue) {
        case FieldDefaultValue::Zero:
            stringValue = "0";
            break;
        case FieldDefaultValue::Infinity:
            return Money::getInfinity();
            break;
        case FieldDefaultValue::NegativeInfinity:
            return Money::getNegativeInfinity();
            break;
        }
    }

    Decimal money(stringValue);
    money = money *= multiplier;
    return money;
}

void FinvizParser::removeInvalidDecimalCharacters(std::string &s)
{
    if(s.find("%") != std::string::npos) {
        s = std::regex_replace(s, std::regex("%"), "");
    }
    if(s.find(",") != std::string::npos) {
        s = std::regex_replace(s, std::regex(","), "");
    }
}
