#ifndef FINVIZPARSERTEST_H
#define FINVIZPARSERTEST_H

#include <string>
#include <QFile>
#include "Company.h"
#include "FinvizParser.h"
#include "Money.h"

class FinvizParserTest : public ::testing::Test
{
public:
    static void SetUpTestCase() {
        QByteArray data = readDataFromTestFile();
        companies = FinvizParser::getCompaniesFromWebPage(data.toStdString());
    }
protected:
    static std::vector<Company> companies;

private:
    static QByteArray readDataFromTestFile() {
        QFile file("C:\\Users\\Max\\dev\\testpage.html");
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            throw std::exception("FinvizParserTest.SetUp: Cannot read input file.");
        }
        return file.readAll();
    }
};

std::vector<Company> FinvizParserTest::companies;

TEST_F(FinvizParserTest, testGetting20CompaniesFromPage)
{
    ASSERT_EQ(companies.size(), 20);
}

TEST_F(FinvizParserTest, testTicker)
{
    Company &company = companies[0];
    ASSERT_EQ("A", company.ticker);
}

TEST_F(FinvizParserTest, testName)
{
    Company &company = companies[0];
    ASSERT_EQ("Agilent Technologies, Inc.", company.name);
}

TEST_F(FinvizParserTest, testSector)
{
    Company &company = companies[0];
    ASSERT_EQ("Healthcare", company.sector);
}

TEST_F(FinvizParserTest, testIndustry)
{
    Company &company = companies[0];
    ASSERT_EQ("Medical Laboratories & Research", company.industry);
}

TEST_F(FinvizParserTest, testCountry)
{
    Company &company = companies[0];
    ASSERT_EQ("USA", company.country);
}

TEST_F(FinvizParserTest, testMarketCapBillion)
{
    Company &company = companies[0];
    Money marketCap("20670000000"); // 20.67B
    ASSERT_EQ(company.marketCap, marketCap);
}

TEST_F(FinvizParserTest, testMarketCapMillion)
{
    Company &company = companies[4];
    Money marketCap("233730000"); // 233.73M
    ASSERT_EQ(company.marketCap, marketCap);
}

TEST_F(FinvizParserTest, testMarketCapDefaultToZero)
{
    Company &company = companies[5];
    Money marketCap("0"); // "-" converted to 0.
    ASSERT_EQ(company.marketCap, marketCap);
}

TEST_F(FinvizParserTest, testPriceToEarnings)
{
    Company &company = companies[0];
    Decimal priceToEarnings("33.19"); // "-" converted to 0.
    ASSERT_EQ(company.priceToEarnings, priceToEarnings);
}

TEST_F(FinvizParserTest, testPriceToEarningsDefaultToInfinity)
{
    Company &company = companies[2];
    Decimal infinity = Decimal::getInfinity();
    ASSERT_EQ(company.priceToEarnings, infinity);
}

TEST_F(FinvizParserTest, testForwardPriceToEarnings)
{
    Company &company = companies[0];
    Decimal forwardPriceToEarnings("24.89");
    ASSERT_EQ(company.forwardPriceToEarnings, forwardPriceToEarnings);
}

TEST_F(FinvizParserTest, testForwardPriceToEarningsDefaultToInfinity)
{
    Company &company = companies[2];
    Decimal infinity = Decimal::getInfinity();
    ASSERT_EQ(company.forwardPriceToEarnings, infinity);
}

TEST_F(FinvizParserTest, testPriceEarningsToGrowth)
{
    Company &company = companies[0];
    Decimal priceEarningsToGrowth("3.11");
    ASSERT_EQ(company.priceEarningsToGrowth, priceEarningsToGrowth);
}

TEST_F(FinvizParserTest, testPriceEarningsToGrowthDefaultToZero)
{
    Company &company = companies[1];
    Decimal zero("0");
    ASSERT_EQ(company.priceEarningsToGrowth, zero);
}

TEST_F(FinvizParserTest, testPriceToSales)
{
    Company &company = companies[0];
    Decimal priceToSales("4.70");
    ASSERT_EQ(company.priceToSales, priceToSales);
}

TEST_F(FinvizParserTest, testPriceToSalesDefaultToInfinity)
{
    Company &company = companies[5];
    Decimal infinity = Decimal::getInfinity();
    ASSERT_EQ(company.priceToSales, infinity);
}

TEST_F(FinvizParserTest, testPriceToBook)
{
    Company &company = companies[0];
    Decimal priceToBook("4.48");
    ASSERT_EQ(company.priceToBook, priceToBook);
}

TEST_F(FinvizParserTest, testPriceToBookDefaultToInfinity)
{
    Company &company = companies[5];
    Decimal infinity = Decimal::getInfinity();
    ASSERT_EQ(company.priceToBook, infinity);
}

TEST_F(FinvizParserTest, testPutCall)
{
    Company &company = companies[0];
    Decimal putCall("8.06");
    ASSERT_EQ(company.putCall, putCall);
}

TEST_F(FinvizParserTest, testPutCallDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.putCall, zero);
}

/* START TO REVIEW */
TEST_F(FinvizParserTest, testPFCF)
{
    Company &company = companies[0];
    Decimal pFCF("41.33");
    ASSERT_EQ(company.pFCF, pFCF);
}

TEST_F(FinvizParserTest, testPFCFDefaultToZero)
{
    Company &company = companies[1];
    Decimal zero("0");
    ASSERT_EQ(company.pFCF, zero);
}

TEST_F(FinvizParserTest, testDividendPercentage)
{
    Company &company = companies[0];
    Decimal dividendPercentage("8.06");
    ASSERT_EQ(company.dividendPercentage, dividendPercentage);
}

TEST_F(FinvizParserTest, testDividendPercentageDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.dividendPercentage, zero);
}

TEST_F(FinvizParserTest, testPayoutRatio)
{
    Company &company = companies[0];
    Decimal payoutRatio("8.06");
    ASSERT_EQ(company.payoutRatio, payoutRatio);
}

TEST_F(FinvizParserTest, testPayoutRatioDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.payoutRatio, zero);
}

TEST_F(FinvizParserTest, testEarningsPerShare)
{
    Company &company = companies[0];
    Decimal earningsPerShare("8.06");
    ASSERT_EQ(company.earningsPerShare, earningsPerShare);
}

TEST_F(FinvizParserTest, testEarningsPerShareDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.earningsPerShare, zero);
}

TEST_F(FinvizParserTest, testEarningsPerShareThisYear)
{
    Company &company = companies[0];
    Decimal earningsPerShareThisYear("8.06");
    ASSERT_EQ(company.earningsPerShareThisYear, earningsPerShareThisYear);
}

TEST_F(FinvizParserTest, testEarningsPerShareThisYearDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.earningsPerShareThisYear, zero);
}

TEST_F(FinvizParserTest, testEarningsPerShareNextYear)
{
    Company &company = companies[0];
    Decimal earningsPerShareNextYear("8.06");
    ASSERT_EQ(company.earningsPerShareNextYear, earningsPerShareNextYear);
}

TEST_F(FinvizParserTest, testEarningsPerShareNextYearDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.earningsPerShareNextYear, zero);
}

TEST_F(FinvizParserTest, testEarningsPerSharePast5Years)
{
    Company &company = companies[0];
    Decimal earningsPerSharePast5Years("8.06");
    ASSERT_EQ(company.earningsPerSharePast5Years, earningsPerSharePast5Years);
}

TEST_F(FinvizParserTest, testEarningsPerSharePast5YearsDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.earningsPerSharePast5Years, zero);
}

TEST_F(FinvizParserTest, testEarningsPerShareNext5Years)
{
    Company &company = companies[0];
    Decimal earningsPerShareNext5Years("8.06");
    ASSERT_EQ(company.earningsPerShareNext5Years, earningsPerShareNext5Years);
}

TEST_F(FinvizParserTest, testEarningsPerShareNext5YearsDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.earningsPerShareNext5Years, zero);
}

TEST_F(FinvizParserTest, testSalesPast5Years)
{
    Company &company = companies[0];
    Decimal salesPast5Years("8.06");
    ASSERT_EQ(company.salesPast5Years, salesPast5Years);
}

TEST_F(FinvizParserTest, testSalesPast5YearsDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.salesPast5Years, zero);
}

TEST_F(FinvizParserTest, testEarningsPerShareQuarterToQuarter)
{
    Company &company = companies[0];
    Decimal earningsPerShareQuarterToQuarter("8.06");
    ASSERT_EQ(company.earningsPerShareQuarterToQuarter, earningsPerShareQuarterToQuarter);
}

TEST_F(FinvizParserTest, testEarningsPerShareQuarterToQuarterDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.earningsPerShareQuarterToQuarter, zero);
}

TEST_F(FinvizParserTest, testSalesQuarterToQuarter)
{
    Company &company = companies[0];
    Decimal salesQuarterToQuarter("8.06");
    ASSERT_EQ(company.salesQuarterToQuarter, salesQuarterToQuarter);
}

TEST_F(FinvizParserTest, testSalesQuarterToQuarterDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.salesQuarterToQuarter, zero);
}

TEST_F(FinvizParserTest, testOutstanding)
{
    Company &company = companies[0];
    Decimal outstanding("8.06");
    ASSERT_EQ(company.outstanding, outstanding);
}

TEST_F(FinvizParserTest, testOutstandingDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.outstanding, zero);
}

TEST_F(FinvizParserTest, testFloatShares)
{
    Company &company = companies[0];
    Decimal floatShares("8.06");
    ASSERT_EQ(company.floatShares, floatShares);
}

TEST_F(FinvizParserTest, testFloatSharesDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.floatShares, zero);
}

TEST_F(FinvizParserTest, testInsiderOwn)
{
    Company &company = companies[0];
    Decimal insiderOwn("8.06");
    ASSERT_EQ(company.insiderOwn, insiderOwn);
}

TEST_F(FinvizParserTest, testInsiderOwnDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.insiderOwn, zero);
}

TEST_F(FinvizParserTest, testInsiderTrans)
{
    Company &company = companies[0];
    Decimal insiderTrans("8.06");
    ASSERT_EQ(company.insiderTrans, insiderTrans);
}

TEST_F(FinvizParserTest, testInsiderTransDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.insiderTrans, zero);
}

TEST_F(FinvizParserTest, testInstOwn)
{
    Company &company = companies[0];
    Decimal instOwn("8.06");
    ASSERT_EQ(company.instOwn, instOwn);
}

TEST_F(FinvizParserTest, testInstOwnDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.instOwn, zero);
}

TEST_F(FinvizParserTest, testInstTrans)
{
    Company &company = companies[0];
    Decimal instTrans("8.06");
    ASSERT_EQ(company.instTrans, instTrans);
}

TEST_F(FinvizParserTest, testInstTransDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.instTrans, zero);
}

TEST_F(FinvizParserTest, testFloatShort)
{
    Company &company = companies[0];
    Decimal floatShort("8.06");
    ASSERT_EQ(company.floatShort, floatShort);
}

TEST_F(FinvizParserTest, testFloatShortDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.floatShort, zero);
}

TEST_F(FinvizParserTest, testShortRatio)
{
    Company &company = companies[0];
    Decimal shortRatio("8.06");
    ASSERT_EQ(company.shortRatio, shortRatio);
}

TEST_F(FinvizParserTest, testShortRatioDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.shortRatio, zero);
}

TEST_F(FinvizParserTest, testReturnOnAssets)
{
    Company &company = companies[0];
    Decimal returnOnAssets("8.06");
    ASSERT_EQ(company.returnOnAssets, returnOnAssets);
}

TEST_F(FinvizParserTest, testReturnOnAssetsDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.returnOnAssets, zero);
}

TEST_F(FinvizParserTest, testReturnOnEquity)
{
    Company &company = companies[0];
    Decimal returnOnEquity("8.06");
    ASSERT_EQ(company.returnOnEquity, returnOnEquity);
}

TEST_F(FinvizParserTest, testReturnOnEquityDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.returnOnEquity, zero);
}

TEST_F(FinvizParserTest, testReturnOnInvestment)
{
    Company &company = companies[0];
    Decimal returnOnInvestment("8.06");
    ASSERT_EQ(company.returnOnInvestment, returnOnInvestment);
}

TEST_F(FinvizParserTest, testReturnOnInvestmentDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.returnOnInvestment, zero);
}

TEST_F(FinvizParserTest, testCurrentRatio)
{
    Company &company = companies[0];
    Decimal currentRatio("8.06");
    ASSERT_EQ(company.currentRatio, currentRatio);
}

TEST_F(FinvizParserTest, testCurrentRatioDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.currentRatio, zero);
}

TEST_F(FinvizParserTest, testQuickRatio)
{
    Company &company = companies[0];
    Decimal quickRatio("8.06");
    ASSERT_EQ(company.quickRatio, quickRatio);
}

TEST_F(FinvizParserTest, testQuickRatioDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.quickRatio, zero);
}

TEST_F(FinvizParserTest, testLongTermDebtToEquity)
{
    Company &company = companies[0];
    Decimal longTermDebtToEquity("8.06");
    ASSERT_EQ(company.longTermDebtToEquity, longTermDebtToEquity);
}

TEST_F(FinvizParserTest, testLongTermDebtToEquityDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.longTermDebtToEquity, zero);
}

TEST_F(FinvizParserTest, testDebtToEquity)
{
    Company &company = companies[0];
    Decimal debtToEquity("8.06");
    ASSERT_EQ(company.debtToEquity, debtToEquity);
}

TEST_F(FinvizParserTest, testDebtToEquityDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.debtToEquity, zero);
}

TEST_F(FinvizParserTest, testGrossM)
{
    Company &company = companies[0];
    Decimal grossM("8.06");
    ASSERT_EQ(company.grossM, grossM);
}

TEST_F(FinvizParserTest, testGrossMDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.grossM, zero);
}

TEST_F(FinvizParserTest, testOperM)
{
    Company &company = companies[0];
    Decimal operM("8.06");
    ASSERT_EQ(company.operM, operM);
}

TEST_F(FinvizParserTest, testOperMDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.operM, zero);
}

TEST_F(FinvizParserTest, testProfitM)
{
    Company &company = companies[0];
    Decimal profitM("8.06");
    ASSERT_EQ(company.profitM, profitM);
}

TEST_F(FinvizParserTest, testProfitMDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.profitM, zero);
}

TEST_F(FinvizParserTest, testPerfWeek)
{
    Company &company = companies[0];
    Decimal perfWeek("8.06");
    ASSERT_EQ(company.perfWeek, perfWeek);
}

TEST_F(FinvizParserTest, testPerfWeekDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.perfWeek, zero);
}

TEST_F(FinvizParserTest, testPerfMonth)
{
    Company &company = companies[0];
    Decimal perfMonth("8.06");
    ASSERT_EQ(company.perfMonth, perfMonth);
}

TEST_F(FinvizParserTest, testPerfMonthDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.perfMonth, zero);
}

TEST_F(FinvizParserTest, testPerfQuart)
{
    Company &company = companies[0];
    Decimal perfQuart("8.06");
    ASSERT_EQ(company.perfQuart, perfQuart);
}

TEST_F(FinvizParserTest, testPerfQuartDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.perfQuart, zero);
}

TEST_F(FinvizParserTest, testPerfHalf)
{
    Company &company = companies[0];
    Decimal perfHalf("8.06");
    ASSERT_EQ(company.perfHalf, perfHalf);
}

TEST_F(FinvizParserTest, testPerfHalfDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.perfHalf, zero);
}

TEST_F(FinvizParserTest, testPerfYear)
{
    Company &company = companies[0];
    Decimal perfYear("8.06");
    ASSERT_EQ(company.perfYear, perfYear);
}

TEST_F(FinvizParserTest, testPerfYearDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.perfYear, zero);
}

TEST_F(FinvizParserTest, testPerfYTD)
{
    Company &company = companies[0];
    Decimal perfYTD("8.06");
    ASSERT_EQ(company.perfYTD, perfYTD);
}

TEST_F(FinvizParserTest, testPerfYTDDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.perfYTD, zero);
}

TEST_F(FinvizParserTest, testBeta)
{
    Company &company = companies[0];
    Decimal beta("8.06");
    ASSERT_EQ(company.beta, beta);
}

TEST_F(FinvizParserTest, testBetaDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.beta, zero);
}

TEST_F(FinvizParserTest, testATR)
{
    Company &company = companies[0];
    Decimal aTR("8.06");
    ASSERT_EQ(company.aTR, aTR);
}

TEST_F(FinvizParserTest, testATRDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.aTR, zero);
}

TEST_F(FinvizParserTest, testVolatilityW)
{
    Company &company = companies[0];
    Decimal volatilityW("8.06");
    ASSERT_EQ(company.volatilityW, volatilityW);
}

TEST_F(FinvizParserTest, testVolatilityWDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.volatilityW, zero);
}

TEST_F(FinvizParserTest, testVolatilityM)
{
    Company &company = companies[0];
    Decimal volatilityM("8.06");
    ASSERT_EQ(company.volatilityM, volatilityM);
}

TEST_F(FinvizParserTest, testVolatilityMDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.volatilityM, zero);
}

TEST_F(FinvizParserTest, testSMA20)
{
    Company &company = companies[0];
    Decimal sMA20("8.06");
    ASSERT_EQ(company.sMA20, sMA20);
}

TEST_F(FinvizParserTest, testSMA20DefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.sMA20, zero);
}

TEST_F(FinvizParserTest, testSMA50)
{
    Company &company = companies[0];
    Decimal sMA50("8.06");
    ASSERT_EQ(company.sMA50, sMA50);
}

TEST_F(FinvizParserTest, testSMA50DefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.sMA50, zero);
}

TEST_F(FinvizParserTest, testSMA200)
{
    Company &company = companies[0];
    Decimal sMA200("8.06");
    ASSERT_EQ(company.sMA200, sMA200);
}

TEST_F(FinvizParserTest, testSMA200DefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.sMA200, zero);
}

TEST_F(FinvizParserTest, testX50DHigh)
{
    Company &company = companies[0];
    Decimal x50DHigh("8.06");
    ASSERT_EQ(company.x50DHigh, x50DHigh);
}

TEST_F(FinvizParserTest, testX50DHighDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.x50DHigh, zero);
}

TEST_F(FinvizParserTest, testX50DLow)
{
    Company &company = companies[0];
    Decimal x50DLow("8.06");
    ASSERT_EQ(company.x50DLow, x50DLow);
}

TEST_F(FinvizParserTest, testX50DLowDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.x50DLow, zero);
}

TEST_F(FinvizParserTest, testX52WHigh)
{
    Company &company = companies[0];
    Decimal x52WHigh("8.06");
    ASSERT_EQ(company.x52WHigh, x52WHigh);
}

TEST_F(FinvizParserTest, testX52WHighDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.x52WHigh, zero);
}

TEST_F(FinvizParserTest, testX52WLow)
{
    Company &company = companies[0];
    Decimal x52WLow("8.06");
    ASSERT_EQ(company.x52WLow, x52WLow);
}

TEST_F(FinvizParserTest, testX52WLowDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.x52WLow, zero);
}

TEST_F(FinvizParserTest, testRSI)
{
    Company &company = companies[0];
    Decimal rSI("8.06");
    ASSERT_EQ(company.rSI, rSI);
}

TEST_F(FinvizParserTest, testRSIDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.rSI, zero);
}

TEST_F(FinvizParserTest, testFromOpen)
{
    Company &company = companies[0];
    Decimal fromOpen("8.06");
    ASSERT_EQ(company.fromOpen, fromOpen);
}

TEST_F(FinvizParserTest, testFromOpenDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.fromOpen, zero);
}

TEST_F(FinvizParserTest, testGap)
{
    Company &company = companies[0];
    Decimal gap("8.06");
    ASSERT_EQ(company.gap, gap);
}

TEST_F(FinvizParserTest, testGapDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.gap, zero);
}

TEST_F(FinvizParserTest, testRecom)
{
    Company &company = companies[0];
    Decimal recom("8.06");
    ASSERT_EQ(company.recom, recom);
}

TEST_F(FinvizParserTest, testRecomDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.recom, zero);
}

TEST_F(FinvizParserTest, testAvgVolume)
{
    Company &company = companies[0];
    Decimal avgVolume("8.06");
    ASSERT_EQ(company.avgVolume, avgVolume);
}

TEST_F(FinvizParserTest, testAvgVolumeDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.avgVolume, zero);
}

TEST_F(FinvizParserTest, testRelVolume)
{
    Company &company = companies[0];
    Decimal relVolume("8.06");
    ASSERT_EQ(company.relVolume, relVolume);
}

TEST_F(FinvizParserTest, testRelVolumeDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.relVolume, zero);
}

TEST_F(FinvizParserTest, testPrice)
{
    Company &company = companies[0];
    Decimal price("8.06");
    ASSERT_EQ(company.price, price);
}

TEST_F(FinvizParserTest, testPriceDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.price, zero);
}

TEST_F(FinvizParserTest, testChange)
{
    Company &company = companies[0];
    Decimal change("8.06");
    ASSERT_EQ(company.change, change);
}

TEST_F(FinvizParserTest, testChangeDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.change, zero);
}

TEST_F(FinvizParserTest, testVolume)
{
    Company &company = companies[0];
    Decimal volume("8.06");
    ASSERT_EQ(company.volume, volume);
}

TEST_F(FinvizParserTest, testVolumeDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.volume, zero);
}

/*TEST_F(FinvizParserTest, testEarnings)
{
    Company &company = companies[0];
    Decimal earnings("8.06");
    ASSERT_EQ(company.earnings, earnings);
}

TEST_F(FinvizParserTest, testEarningsDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.earnings, zero);
}*/

TEST_F(FinvizParserTest, testTargetPrice)
{
    Company &company = companies[0];
    Decimal targetPrice("8.06");
    ASSERT_EQ(company.targetPrice, targetPrice);
}

TEST_F(FinvizParserTest, testTargetPriceDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.targetPrice, zero);
}

/*TEST_F(FinvizParserTest, testIPODate)
{
    Company &company = companies[0];
    Decimal iPODate("8.06");
    ASSERT_EQ(company.iPODate, iPODate);
}

TEST_F(FinvizParserTest, testIPODateDefaultToZero)
{
    Company &company = companies[5];
    Decimal zero("0");
    ASSERT_EQ(company.iPODate, zero);
}*/

/* END TO REVIEW */

#endif // FINVIZPARSERTEST_H
