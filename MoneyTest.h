#ifndef MONEYTEST_H
#define MONEYTEST_H

#include "gtest/gtest.h"
#include "Money.h"

class MoneyTest : public ::testing::Test
{
};

TEST_F(MoneyTest, testBlankThrowsException)
{
    ASSERT_THROW(Money money(""), std::exception);
}

TEST_F(MoneyTest, testDashOnlyThrowsException)
{
    ASSERT_THROW(Money money("-"), std::exception);
}

TEST_F(MoneyTest, testNonDigitThrowsException)
{
    ASSERT_THROW(Money money("20.67B"), std::exception);
}

#endif // MONEYTEST_H
