#ifndef FINVIZDOWNLOADER_H
#define FINVIZDOWNLOADER_H

#include <string>

class FinvizDownloader
{
public:
    static std::string getWebPage(size_t offset);
    static size_t getPageCount();
private:
    static std::string finvizUrl;
};

#endif // FINVIZDOWNLOADER_H
