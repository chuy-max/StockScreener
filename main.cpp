#include <QApplication>
#include <iostream>
#include "StockScreener.h"
#include "gtest/gtest.h"
#include "curl/curl.h"

#include "FinvizDownloaderTest.h"
#include "FinvizParserTest.h"
#include "MoneyTest.h"
#include "CompanyDatabaseTest.h"
#include "FinvizScraper.h"

int runTests(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

int runApp(int argc, char *argv[])
{
    try {
        QApplication a(argc, argv);
        StockScreener w;
        w.show();

        return a.exec();
    }
    catch (std::exception &e) {
       std::cerr << e.what() << std::endl;
    }
    return -1;
}

void scrapeFinviz()
{
    std::vector<Company> companies = FinvizScraper::getTodaysCompanyData();
    CompanyDatabase().store(companies);
}

void printCompanies()
{
    std::vector<Company> companies = CompanyDatabase().load();
    for(int i=0; i<companies.size(); i++) {
        printf("Company: %d, %s\n", i, companies[i].toString().c_str());
    }
}

int main(int argc, char *argv[])
{
    runApp(argc, argv);
    //scrapeFinviz();
    //printCompanies();
/*    try {
        return runTests(argc, argv);
    }
    catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }*/
}
