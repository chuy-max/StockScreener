#ifndef STOCKSCREENER_H
#define STOCKSCREENER_H

#include <QMainWindow>

namespace Ui {
class StockScreener;
}

class StockScreener : public QMainWindow
{
    Q_OBJECT

public:
    explicit StockScreener(QWidget *parent = 0);
    ~StockScreener();

private:
    Ui::StockScreener *ui;
};

#endif // STOCKSCREENER_H
