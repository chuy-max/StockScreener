#ifndef COMPANYDATABASE_H
#define COMPANYDATABASE_H

#include <vector>
#include "Company.h"

class CompanyDatabase
{
public:
    void store(const std::vector<Company> &companies);
    std::vector<Company> load();
};

#endif // COMPANYDATABASE_H
