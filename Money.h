#ifndef MONEY_H
#define MONEY_H

#include "decimal.h"

class Money : public DEC_NAMESPACE::decimal<4>
{
public:
    Money();
    Money(std::string decimalValue);
    bool operator==(const Money &rhs) const;
    Money& operator*=(const Money &rhs);

    static Money getInfinity();
    static Money getNegativeInfinity();
private:
    template <class Archive> friend void serialize(Archive &ar, Money &m);
    long long& getRawDecimal();
};

template <class Archive>
void serialize(Archive & ar,
               Money & m)
{
    ar(m.getRawDecimal());
}

class MoneyException : public std::exception
{
public:
    MoneyException(std::string message) noexcept :
        std::exception(),
        message(message)
    {
    }

    virtual const char* what() const noexcept override
    {
        return message.c_str();
    }
private:
    std::string message;
};

typedef Money Decimal;

#endif // MONEY_H
