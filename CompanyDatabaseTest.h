#ifndef COMPANYDATABASETEST_H
#define COMPANYDATABASETEST_H

#include "gtest/gtest.h"
#include "Company.h"
#include "CompanyDatabase.h"

class CompanyDatabaseTest : public ::testing::Test
{
protected:
    virtual void SetUp()
    {
        c1.ticker = "GOOG";
        c1.price = Decimal("100");
        c1.currentRatio = Decimal("200");
        c1.iPODate = "10/10/2017";
        std::vector<Company> companies, retrievedCompanies;
        companies.push_back(c1);

        CompanyDatabase().store(companies);
        retrievedCompanies = CompanyDatabase().load();

        c2 = retrievedCompanies[0];
    }
    Company c1, c2;
};

TEST_F(CompanyDatabaseTest, testCompanyTicker)
{
    ASSERT_EQ(c2.ticker, "GOOG");
}
TEST_F(CompanyDatabaseTest, testCompanyPrice)
{
    ASSERT_EQ(c2.price, Decimal("100"));
}
TEST_F(CompanyDatabaseTest, testCompanyCurrentRatio)
{
    ASSERT_EQ(c2.currentRatio, Decimal("200"));
}
TEST_F(CompanyDatabaseTest, testCompanyIPODate)
{
    ASSERT_EQ(c2.iPODate, "10/10/2017");
}

#endif // COMPANYDATABASETEST_H
