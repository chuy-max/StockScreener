#ifndef FINVIZDOWNLOADERTEST_H
#define FINVIZDOWNLOADERTEST_H

#include "FinvizDownloader.h"

class FinvizDownloaderTest : public ::testing::Test
{
};

TEST_F(FinvizDownloaderTest, DownloadMoreThan1KB)
{
    std::string webPage = FinvizDownloader::getWebPage(0);
    ASSERT_GT(webPage.size(), 1024);
}

#endif // FINVIZDOWNLOADERTEST_H
