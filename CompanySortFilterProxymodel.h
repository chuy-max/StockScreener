#ifndef COMPANIESSORTFILTERPROXYMODEL_H
#define COMPANIESSORTFILTERPROXYMODEL_H

#include <QSortFilterProxyModel>

class CompanySortFilterProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    CompanySortFilterProxyModel(QObject *parent);

    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
    bool filterAcceptsRow(int sourceRow,
                          const QModelIndex &sourceParent) const override;

public slots:
    void setMaxPE(double newMaxPE);
    void setMinCurrentRatio(double newMinCurrentRatio);
    void setMinDividend(double newMinDividend);
    void setMaxPB(double newMaxPB);

private:
    double maxPE;
    double minCurrentRatio;
    double minDividend;
    double maxPB;
};

#endif // COMPANIESSORTFILTERPROXYMODEL_H
