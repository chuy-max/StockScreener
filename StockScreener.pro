#-------------------------------------------------
#
# Project created by QtCreator 2017-04-08T11:16:35
#
#-------------------------------------------------

QT += core gui
QT += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = StockScreener
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
    StockScreener.cpp \
    CompanyTableModel.cpp \
    CompanySortFilterProxyModel.cpp \
    FinvizScraper.cpp \
    CompanyTableView.cpp \
    FinvizDownloader.cpp \
    FinvizParser.cpp \
    Money.cpp \
    CompanyDatabase.cpp \
    CompanyMapper.cpp \
    CompanyTableViewItemStyleDelegate.cpp

HEADERS  += \
    Company.h \
    Money.h \
    StockScreener.h \
    CompanyTableModel.h \
    CompanySortFilterProxymodel.h \
    FinvizScraper.h \
    CompanyTableView.h \
    FinvizDownloader.h \
    FinvizParser.h \
    FinvizDownloaderTest.h \
    FinvizParserTest.h \
    FinvizField.h \
    MoneyTest.h \
    CompanyDatabase.h \
    CompanyDatabaseTest.h \
    CompanyMapper.h \
    CompanyTableViewItemStyleDelegate.h

FORMS    += stockscreener.ui

RC_ICONS = emblem_money.ico

# googletest
unix|win32: LIBS += -L$$PWD/../googletest/googlemock/gtest/Debug/ -lgtest
INCLUDEPATH += $$PWD/../googletest/googletest/include
DEPENDPATH += $$PWD/../googletest/googletest/include

# libcurl
unix|win32: LIBS += -L$$PWD/../curl/builds/libcurl-vc-x64-debug-dll-ipv6-sspi-winssl/lib/ -llibcurl_debug
INCLUDEPATH += $$PWD/../curl/include
DEPENDPATH += $$PWD/../curl/include

# libcurlpp
unix|win32: LIBS += -L$$PWD/../curlpp/Debug/ -llibcurlpp
INCLUDEPATH += $$PWD/../curlpp/include
DEPENDPATH += $$PWD/../curlpp/include

# gumbo-parser
unix|win32: LIBS += -L$$PWD/../gumbo-parser/visualc/x64/Debug/ -lgumbo
INCLUDEPATH += $$PWD/../gumbo-parser/src
DEPENDPATH += $$PWD/../gumbo-parser/src

# gumbo-query
unix|win32: LIBS += -L$$PWD/../gumbo-query/lib/Debug/ -lgq
INCLUDEPATH += $$PWD/../gumbo-query/src
DEPENDPATH += $$PWD/../gumbo-query/src

# decimal_for_cpp
INCLUDEPATH += $$PWD/../decimal_for_cpp/include
DEPENDPATH += $$PWD/../decimal_for_cpp/include

# cereal
INCLUDEPATH += $$PWD/../cereal/include
