#include "CompanySortFilterProxymodel.h"
#include "Company.h"

CompanySortFilterProxyModel::CompanySortFilterProxyModel(QObject *parent) :
    QSortFilterProxyModel(parent),
    maxPE(INFINITY),
    minCurrentRatio(0),
    minDividend(0),
    maxPB(INFINITY)
{
}

QVariant CompanySortFilterProxyModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole) {
        if (orientation == Qt::Vertical) {
            return section+1;
        }
    }
    return QSortFilterProxyModel::headerData(section, orientation, role);
}

bool CompanySortFilterProxyModel::filterAcceptsRow(int sourceRow,
                                                     const QModelIndex &sourceParent) const
{
    QModelIndex peIndex = sourceModel()->index(sourceRow, CompanyFields::PriceToEarnings, sourceParent);
    double pe = sourceModel()->data(peIndex).toDouble();

    QModelIndex currentRatioIndex = sourceModel()->index(sourceRow, CompanyFields::CurrentRatio, sourceParent);
    double current_ratio = sourceModel()->data(currentRatioIndex).toDouble();

    QModelIndex dividendIndex = sourceModel()->index(sourceRow, CompanyFields::DividendPercentage, sourceParent);
    double dividend = sourceModel()->data(dividendIndex).toDouble();

    QModelIndex pbIndex = sourceModel()->index(sourceRow, CompanyFields::PriceToBook, sourceParent);
    double pb = sourceModel()->data(pbIndex).toDouble();

    bool acceptsRow = true;

    if(pe > maxPE)
        acceptsRow = false;
    if(current_ratio < minCurrentRatio)
        acceptsRow = false;
    if(dividend < minDividend)
        acceptsRow = false;
    if(pb > maxPB)
        acceptsRow = false;

    return acceptsRow;
}

void CompanySortFilterProxyModel::setMaxPE(double newMaxPE)
{
    maxPE = newMaxPE;
    invalidate();
}

void CompanySortFilterProxyModel::setMinCurrentRatio(double newMinCurrentRatio)
{
    minCurrentRatio = newMinCurrentRatio;
    invalidate();
}

void CompanySortFilterProxyModel::setMinDividend(double newMinDividend)
{
    minDividend = newMinDividend;
    invalidate();
}

void CompanySortFilterProxyModel::setMaxPB(double newMaxPB)
{
    maxPB = newMaxPB;
    invalidate();
}
